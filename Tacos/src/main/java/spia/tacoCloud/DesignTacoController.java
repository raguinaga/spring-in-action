package spia.tacoCloud;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import spia.tacoCloud.Ingredient.Type;

import java.util.Arrays;
import java.util.List;

/**
 * This class Handles HTTP get requests where the path request is /design
 * It Builds a list of ingredients
 * Hands off the request and ingredient data to a view template to be rendered as HTML and sent to the
 * requesting browser
 */

@Slf4j // creates a static logging property for this class
@Controller
@RequestMapping("/design")
@SessionAttributes("tacoOrder") // Indicates that the tacoOrder created in the class should be maintained in session
public class DesignTacoController {
    
    // Builds a list of ingredients
    @ModelAttribute
    public void addIngredientsToModel(Model model) {
        var ingredients = Arrays.asList(
                new Ingredient("FLTO", "Flour Tortilla", Type.WRAP),
                new Ingredient("COTO", "Corn Tortilla", Type.WRAP),
                new Ingredient("GRBF", "Ground Beef", Type.PROTEIN),
                new Ingredient("CARN", "Carnitas", Type.PROTEIN),
                new Ingredient("TMTO", "Diced Tomatoes", Type.VEGGIES),
                new Ingredient("LETC", "Lettuce", Type.VEGGIES),
                new Ingredient("CHDR", "Cheddar", Type.CHEESE),
                new Ingredient("JACK", "Monterrey Jack", Type.CHEESE),
                new Ingredient("SLSA", "Salsa", Type.SAUCE),
                new Ingredient("SRCR", "Sour Cream", Type.SAUCE)
        );
        
        
        var types = Ingredient.Type.values();
        for (Type type : types) {
            model.addAttribute(type.toString().toLowerCase(),
                               filterByType(ingredients, type));
        }
    }
    
    @ModelAttribute(name = "tacoOrder")
    public TacoOrder order() {
        return new TacoOrder();
    }
    
    @ModelAttribute(name = "taco")
    public Taco taco() {
        return new Taco();
    }
    
    /* Refines the class level Request Mapping annotation. It specifies that when the request is a GET
    * request, this method will handle the request. However, this method also makes a new model, calling
    * the request for a new taco and taco order and constructing an ingredients list as seen above */
    @GetMapping
    public String showDesignForm() {
        return "design";
    }
    
    /* Adding a post method to handle post requests */
    @PostMapping
    public String processTaco(Taco taco, @ModelAttribute TacoOrder order) {
        order.addTaco(taco);
        log.info("Processing taco: " + taco);
        return "redirect:/orders/current";
    }
    
    private Iterable<Ingredient> filterByType(List<Ingredient> ingredients, Type type) {
        return ingredients.stream()
                          .filter(ingredient -> ingredient.type().equals(type))
                          .toList();
    }
}

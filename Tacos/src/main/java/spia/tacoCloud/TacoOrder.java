package spia.tacoCloud;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/** This class models a taco order*/
@Data
public class TacoOrder implements Serializable {
    private String deliveryName;
    private String deliveryStreet;
    private String deliveryCity;
    private String deliveryState;
    private String deliveryZip;
    private String ccNumber;
    private String ccExpDate;
    private String cc_CVV;
    
    private List<Taco> tacos = new ArrayList<>();
    
    /** Adds a taco to the order list
     * @param taco a taco to add to the order
     */
    public void addTaco(Taco taco) {
        this.tacos.add(taco);
    }
}
